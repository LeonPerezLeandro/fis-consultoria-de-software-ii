## Nearsoft
![Logo Nearsoft](imagenes/Nearsoft.svg)
### Link
[Nearsoft.com](https://nearsoft.com/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Nearsoft+Inc.+DF/@19.4130209,-99.1644572,15z/data=!4m5!3m4!1s0x0:0x1ffdbfbfc6116db5!8m2!3d19.4130406!4d-99.1644615?hl=es-419)
### Acerca de
Nearsoft tiene que ver con el desarrollo de software. 
Nuestros equipos se autogestionan y entregan a tiempo. 
Tratamos nuestro trabajo como un oficio y aprendemos unos de otros. 
Constantemente subimos el listón.
### Servicios:
- Desarrollo web.
- Desarrollo de productos.
- Desarrollo de aplicaciones móviles
- Desarrollo IoT.
- Desarrollo de software en general.
### Presencia
- [Facebook](https://www.facebook.com/Nearsoft/)
- [Twitter](https://twitter.com/Nearsoft)
### Ofertas laborales
[Trabajos](https://www.linkedin.com/company/nearsoft/)
### Blog
[Blog](https://nearsoft.com/blog/)
### Tecnologías
- Kotlin
- Swift
- Python
- JavaScript
- HTML5
- CSS3
## Grow IT 
![Logo Grow IT](imagenes/growit.png)
### Link
[Grow IT.com](http://www.grw.com.mx)
### Ubicación
[Ubicación](https://www.google.com/maps/@19.377924,-99.175947,13z?hl=es-US)
### Acerca de
Somos un equipo joven y dinámico conformado por profesionales del software que entiende la trascendencia de la cuarta revolución industrial y el apoyo que para las diversas industrias representan las aplicaciones empresariales de Microsoft nuestra área de especialidad.
### Servicios:
- Conector NOM2001 D365
- Cobro de tarjeta MIT en POS
- Repositorio de clientes Digitales
- Factura electrónica para
- Dynamics SL
- Conector Sigein
### Presencia
- [Facebook](https://www.facebook.com/growitc)
- [Instagram](https://www.instagram.com/grow_it_mx/)
### Ofertas laborales
[Link](https://www.occ.com.mx/empresas/hirer-center/actividad/)
### Blog
[Blog](https://blog.contpaqi.com/ )
### Tecnologías
- JavaScrip
- HTML
- CSS
- Kotlin
- Swift

## Scio Consulting
![Logo Scio](imagenes/scio.png)
### Link
[Scio.com](http://www.scio.com.mx)
### Ubicación
[Ubicación](https://goo.gl/maps/8QoeZZWi8LLMvTrh9S)
### Acerca de
Nos dedicamos al desarrollo de soluciones web, en la nube y SaaS, así como también soporte.
Trabajamos con nuestros clientes para construir y operar aplicaciones y servicios confiables. Adoptamos un enfoque de ciclo de vida completo, asociándonos con nuestros clientes desde la conceptualización, pasando por el desarrollo, hasta el mantenimiento, soporte y DevOps en curso.  
### Servicios:
 - Aplicaciones Web  
 - Aplicaciones Móviles  
 - Consultoría Tecnológica  
 - Servicios en la Nuve  
 - Desarrollo de aplicaciones de Negocio  
 - Soluciones de Software  

### Presencia
- [Facebook](https://www.facebook.com/ScioMx)
- [Twitter](https://twitter.com/sciomx)
### Ofertas laborales
[Trabajos](https://www.scio.com.mx/trabaja-scio-mexico/vacantes/)
### Blog
[Blog](https://www.scio.com.mx/blog/)
### Tecnologías
 - React Native  
 - Node  
 - Java  
 - HTML  
 - Kotlin  
 - Swift  
 - Ruby on Rails  
 - JavaScript  
 - React  
 - CSS  

## Bitso
![Logo bitso](imagenes/bitso.png)
### Link
[bitso](https://bitso.com/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Oficinas+bitso/@19.4295734,-99.2052136,17z/data=!3m1!4b1!4m5!3m4!1s0x85d201f7d9e50377:0xe421fec07501d537!8m2!3d19.4295734!4d-99.2030249)
### Acerca de
Bitso es una plataforma para comprar, vender y usar criptomonedas, con operaciones en Argentina, Brasil y México. Fundada en 2014 por Ben Peters y Pablo González, con la incorporación de Daniel Vogel en 2015 como socio igualitario.
### Servicios 
- Compra y venta de criptomonedas, criptodivizas y criptocurrency en general
### Presencia
- [Facebook](https://www.facebook.com/bitsoex)
- [Twitter](https://twitter.com/bitso)
- [Instagram](https://instagram.com/bitso)
- [YouTube](https://www.youtube.com/bitso)
- [Telegram](https://t.me/Bitso_Mexico)
### Ofertas laborales
[Trabajos](https://bitso.com/jobs)
### Blog
[Blog](https://blog.bitso.com/)
### Tecnologías
- Kotlin
- Java
- Scrum
- JavaScript
- FLutter
- Swift
- Android
- IOS

## Unosquare
![Logo Unosquare](imagenes/unosquare.svg)
### Link
[Unosquare](https://www.unosquare.com)
### Ubicación
[Ubicación](https://goo.gl/maps/eqMVaftkv55hfDpKA)
### Acerca de
Unosquare está aquí para crear un legado. Tenemos el compromiso de generar bienestar en todas las geografías en las que tenemos presencia. Nuestra organización ha sido bendecida con la oportunidad de impactar sustancialmente el espíritu en el que continuamos desarrollando la empresa. Creemos en un esfuerzo global hacia la igualdad de oportunidades.    
### Servicios 
- Desarrollo ágil de software  
- Servicios de desarrollo Nearshore  
- Consultoría de proyectos de tecnología  
- Estrategias de transformación digital  
- Aumento ágil distribuido  
- Consultoría de proyectos de tecnología  
- Transformación digital   

### Presencia
- [Twitter](https://twitter.com/unosquare)  
- [LinkedIn](https://www.linkedin.com/company/293703/)  
- [Facebook](https://www.facebook.com/unosquare/)  
### Ofertas laborales
[Trabajos](https://people.unosquare.com/#welcome)
### Blog
[Blog](https://blog.unosquare.com)
### Tecnologías
- Chef  
- Hadoop  
- MySQL  
- Postgres  
- Elastic Search  
- VMware vSphere  
- Juniper  
- HP ISS  
- HP 3Par  
- F5 LTM  
- Nagios  
- Cacti  
- SQL  
- PL/SQL  
- React/Redux CSS  
- PHP  
- Ruby  
- Python  
- Rust  
- Node   
- .NET  
- Git  
- SVN  
- Docker  
- Zephyr  

## iTexico
![Logo iTexico.io](imagenes/itexico.jpg)
### Link
[iTexico](https://www.itexico.com/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Improving/@20.6899988,-103.4162525,18.25z/data=!3m1!5s0x8428aeee0bfb0ac7:0xbf1155c67199fe4c!4m12!1m6!3m5!1s0x8428add6c3c1323f:0x1b33ebadfe7f1b2f!2sImproving!8m2!3d20.6898348!4d-103.4155984!3m4!1s0x8428add6c3c1323f:0x1b33ebadfe7f1b2f!8m2!3d20.6898348!4d-103.4155984)
### Acerca de
Impulsamos la innovación digital que permite a las empresas transformar su negocio a través de una amplia gama de ofertas de servicios digitales que incluyen: diseño, ingeniería de productos, control de calidad, dispositivos móviles, nube e inteligencia artificial.
### Servicios 
- UI/UX Design
- Software Development Services
- QA
- Moible Software Development
- CLoud
- A.I.
- .NET
- Desktop Development
- DevOps
### Presencia
- [Facebook](https://www.facebook.com/itexico)
- [Twitter](https://twitter.com/improvingmx)
- [YouTube](https://www.youtube.com/channel/UCuYem4j89E0HbApDJJnyDlw)
### Ofertas laborales
[Trabajos](https://www.itexico.com/careers)
### Blog
[Blog](https://www.itexico.com/blog)
### Tecnologías
- JavaScript
- Java
- Kotlin
- .NET
- Spring
- Flutter

## SERTI
![Logo SERTI.io](imagenes/serti.png)
### Link
[SERTI](https://serti.com.mx/)
### Ubicación
[Ubicación](https://goo.gl/maps/X1RFKqeVZM5LyfWD8)
### Acerca de
Ofrecemos desarrollo de software basado en productos o servicios Web, utilizando ciclos de desarrollo “agiles” que incluyan un fuerte análisis, diseño, desarrollo, pruebas y en caso de necesitarlo mantenimiento, nos gusta desarrollar software web con un alto contenido de User-Experience, pantallas fáciles de utilizar esto ayudara no solo a tus usuarios si no al modelo de tu negocio.
### Servicios 
- Desarrollo de plataformas web
- Desarrollo gráfico
- Desarrollo de apps
- Reclutamiento IT
### Presencia
- [Facebook](https://www.facebook.com/SERTIMX/)
- [Twitter](https://twitter.com/serTI_Mx)
### Ofertas laborales
[Trabajos](https://serti.com.mx/unete/)
### Blog
[Blog](https://serti.com.mx/serti/)
### Tecnologías
- Java
- Spring Web
- .NET
- C++
- GlassFish
- Spring Batch
- Bootstrap
- Spring Security
- MySQL
- HIGHCHARTS
- HIBERNATE
- HTML5
- CSS
- JS
- WordPress
- WooCommerce
 -PS

## IcaliaLabs
![Logo IcaliaLabs.io](imagenes/IcaliaLabs.png)
### Link
[IcaliaLabs](https://www.icalialabs.com)
### Ubicación
[Ubicación](https://goo.gl/maps/m3hHarvqVYgL9iGEA)
### Acerca de
En Icalia Labs transformamos negocios con soluciones digitales poderosas y adaptables que satisfacen las necesidades de hoy y desbloquean las oportunidades del mañana.  
### Servicios 
- Desarrollo de software personalizado  
- Desarrollo web y móvil  
- Modernización de software heredado  
- Diseño de producto Sprint  
- Taller de diseño de producto  
- Evaluación de tecnología  
- Estrategia de innnovación  
- Dotación de personal remota  

### Presencia
- [GitHub](https://github.com/IcaliaLabs)  
- [dribbble](https://dribbble.com/icalialabs/members)  
- [LinkedIn](https://www.linkedin.com/company/icalia-labs/)  

### Ofertas laborales
[Trabajos](https://www.icalialabs.com/open-positions/digital-designer)
### Blog
[Blog](https://www.icalialabs.com/blog-software-development-tips-and-news)
### Tecnologías
- Figma  
- Sketch  
- Adobe Illustrator  
- Adobe Photoshop  
- Adobe XD
- InVision
- CSS  
- HTML  
- Java  
- Ruby on Rails  
- Microsoft .NET  
- C++  
- JavaScript  
- SDLC  
- JQuery  
- Sass  
- Less  
- Furatto  
- BootStrap  
- Foundation  

## AVIADA
![Logo AVIADA.io](imagenes/aviada.png)
### Link
[AVIADA](https://aviada.mx/)
### Ubicación
[Ubicación](https://goo.gl/maps/DVUjdTUuPZ9uXsmq8)
### Acerca de
Aviada es una empresa de nearshoring y offshoring en México para todas sus necesidades de aumento de personal. Nos especializamos en encontrar y cuidar a grandes empleados en los campos de desarrollo de software, medios digitales, diseño multimedia y otros.
### Servicios 
- Recruitment
- Legal Compliance
- Payroll Processing
- HR
- Procurement
- Equipment Leasing

### Presencia
- [Facebook](https://www.facebook.com/aviadamx)
- [Twitter](https://twitter.com/AviadaMX)
- [Instagram](https://www.instagram.com/aviadamx/)
### Ofertas laborales
[Trabajos](https://aviada.mx/trabaja-con-aviada/)
### Blog
[Blog](https://aviada.mx/category/blog/)
### Tecnologías
- Android
- Angular
- AWS/CLOUD
- Data Science
- DevOps
- E-Commerce
- Javascript
- Joomla CMS
- Laravel
- NodeJS
- PHP
- Python
- QA
- React
- React Native
- Swift
- Unity
- Wordpress
- Xamarin
- 3D Animation
- Accessibility (WCAG 2.0)
- Account Management
- Ad Ops / Trafficking
- Ad Bidding 
- AMP
- Campaign Management
- Customer Service
- Graphic Design
- Finance / Accounts Receivable
- Marketing
- Project Management
- Sales Support
- Technical (Web) Support
- UI/UX
- Salesforce Admin
- Video Editing

## Platzi
![Logo Platzi](imagenes/platzi.png)
### Link
[Platzi](https://platzi.com/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/michelada.io/@19.266788,-103.717539,15z/data=!4m5!3m4!1s0x0:0x3dbaa3579153065f!8m2!3d19.266788!4d-103.717539?hl=es-419)
### Acerca de
Platzi es una plataforma latinoamericana de educación en línea.  Fue fundada en 2014 por el ingeniero colombiano Freddy Vega y por el informático guatemalteco Christian Van Der Henst.
### Servicios 
- Cursos online
### Presencia
- [Facebook](https://facebook.com/platzi)
- [Twitter](https://twitter.com/platzi)
- [Instagram](https://www.instagram.com/platzi/)
- [YouTube](https://www.youtube.com/channel/UC55-mxUj5Nj3niXFReG44OQ)
- [TikTok](https://tiktok.com/@aprendeconplatzi)
- [Spotify](https://open.spotify.com/show/66phcUoQsM3URyzhFDz9ig)
### Ofertas laborales
[Trabajos](https://linkedin.com/school/platzi-inc)
### Blog
[Blog](https://platzi.com/blog/)
### Tecnologías
- JavaScrip
- HTML
- CSS
- Kotlin
- Swift

## MagmaLabs
![Logo MagmaLabs](imagenes/magmalabs.png)
### Link
[MagmaLabs](https://www.magmalabs.io)
### Ubicación
[Ubicación](https://goo.gl/maps/saCUFEZj6zoPnAA4A)
### Acerca de
Somos una agencia de desarrollo de software estadounidense con operaciones en México y otras partes de América Latina. Desde el inicio, nuestra misión ha sido impulsar la innovación mientras desarrollamos talento humano de clase mundial.  
Durante los últimos diez años, hemos trabajado en más de cien proyectos para clientes de todo el mundo y nos hemos ganado una reputación notable como especialistas en comercio electrónico.  
### Servicios 
- Diseño Web  
- Desarrollo de software personalizado  

### Presencia
- [Twitter](https://twitter.com/wearemagmalabs)  
- [Facebook](https://www.facebook.com/magmalabsio/)  
- [LinkedIn](https://mx.linkedin.com/company/magmalabs)  

### Ofertas laborales
[Trabajos](https://magmalabs.bamboohr.com/jobs/)
### Blog
[Blog](http://blog.magmalabs.io)
### Tecnologías
- Ruby  
- React  
- React Native  
- Node JS    

## SIMPAT TECH
![Logo SIMPAT TECH](imagenes/simplet.png)
### Link
[SIMPAT TECH](https://simpat.tech/)
### Ubicación
[Ubicación](https://goo.gl/maps/nvX2gKpMJNCjJXaa9)
### Acerca de
Es una empresa de consultoría de software personalizado con sede en Austin, Texas, con un centro de distribución de software de última generación en Monterrey, Mexico. Somos un equipo de profesionales de software innovadores que buscan alcanzar los desafíos más complejos y las metas de misión crítica de nuestros clientes. 
### Servicios 
- Software personalizado
- DevOps
- Soluciones Cloud
- Equipos mejorados
- Seguro de calidad
- Inteligencia de negocio

### Presencia
- [Twitter](twitter.com/simpat_tech)  
- [Facebook](https://www.facebook.com/simpat.tech/)  

### Ofertas laborales
[Trabajos](https://simpat.tech/careers/)
### Blog
[Blog](https://simpat.tech/insights/)
### Tecnologías
- Azure
- AWS
- Kubernetes
- Docker
- PAaS
- RabbitMQ
- Microservices
- Cloud services

## Tiempo Development
![Logo Tiempo Development](imagenes/TiempoDevelopment.png)
### Link
[Tiempo Development](https://www.tiempodev.com)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Tiempo+Development+Monterrey/@20.6779413,-108.7097206,5z/data=!4m9!1m2!2m1!1sTiempo+Development!3m5!1s0x8662bfbbe1227fd7:0xfa04fc0360c31a08!8m2!3d25.646523!4d-100.2916083!15sChJUaWVtcG8gRGV2ZWxvcG1lbnQiA4gBAZIBEGNvcnBvcmF0ZV9vZmZpY2U?hl=es-419)
### Acerca de
Equipos de alto rendimiento distribuidos globalmente
Tiempo es ampliamente reconocida como una de las principales empresas de ingeniería de software de EE. UU. 
Poniendo a trabajar nuestros recursos de ingeniería independientes de la costa y equipos de alto 
rendimiento con un enfoque incansable en los resultados del cliente, Tiempo diseña, construye e 
implementa software que deleita a los clientes.

### Servicios 
- Análisis técnico
- Arquitectura de software
- Diseño UI/UX
- Product management
- Soporte y mantenimiento de aplicaciones
- Tests de calidad 
- Desarrollo de aplicaciones web
- Desarrollo de aplicaciones móviles
- Integración con Apps/API
- Data Science
- IoT
- Microservicios
- DevOps
- Tests de automatización
- Cloud Native
- Cloud Cost Management
- Data Science
- Cloud Migration Services

### Presencia
- [Twitter](https://twitter.com/TiempoSoftware)  

### Ofertas laborales
[Trabajos](https://mx.linkedin.com/company/tiempo-development)
### Blog
[Blog](https://twitter.com/TiempoSoftware)
### Tecnologías
- Java
- Kotlin
- Objective-C
- Xamarin
- Swift
- Adobe PhoneGap
- React
- CSS3
- HTML5
- JavaScript
- Angular.JS
- Microsoft.NET
- Python
- Flask
- Django
- Node.js
- Express
- PHP
- Laravel
- C
- AWS
- Azure
- Google CLoud
- LAMP
- Falcon
- Tornado
- Kafka
- Kubernetes
- Spring boot
- Docker
- Git
- Trello
- MySQL
 

## Tacit knowledge
![Logo Tacit knowledge](imagenes/tacit.png)
### Link
[Tacit knowledge](https://www.tacitknowledge.com)
### Ubicación
[Ubicación](https://goo.gl/maps/AFYGitj8yDeWmSBh6)
### Acerca de
Tacit Knowledge crea, integra y respalda software empresarial para marcas globales. El enfoque principal de Tacit es el comercio digital y hemos ganado varios premios por nuestro trabajo en esta área. Nuestra experiencia internacional se extiende a implementaciones dentro del comercio móvil, comercio social y de gran escala. Nuestro historial de clase mundial se deriva de la especialización en tecnologías clave y equipos de campo con amplia experiencia en el dominio. También ofrecemos servicios de consultoría en selección de productos, diseño creativo y UX, coaching ágil, estabilización de sistemas y ajuste del rendimiento. Y, por último, ofrecemos soporte de aplicaciones "follow the sun" las 24 horas del día, los 7 días de la semana, monitoreo y alertas de clase mundial y gestión de incidentes.  

### Servicios 
- Consultoría de software  
- Consultoría Ecommerce  
- Comercio digital  
- Servicios gestionados  
- Peak Ready performance  
- Comercio SAP  
- NCommerce    


### Presencia
- [Twitter](https://twitter.com/tacitknowledge)  

### Ofertas laborales
[Trabajos](https://www.tacitknowledge.com/careers)
### Blog
[Blog](https://www.tacitknowledge.com/thoughts/)
### Tecnologías
- AWS  
- GCP  
- Azure  
- Docker  
- Kubernetes  
- HTTP  
- Nginx  
- Apache  
- MySQL  
- MSSQL  
- PostgreSQL  
- Ruby  
- Python  
- Make  
- Ant  
- Maven  
- Rake  

## SVITLA
![Logo SVITLA](imagenes/svilita.png)
### Link
[Tacit SVITLA](https://svitla.com/)
### Ubicación
[Ubicación](https://goo.gl/maps/sHoHGjSpi6u1Wa8a6)
### Acerca de
Somos su conducto hacia las innovaciones tecnológicas más punteras. Ofrecemos un valor incomparable a nuestros clientes, que confían en nuestra experiencia y muchos años de experiencia en Managed Team Extension y AgileSquads.

### Servicios 
- Extensión de equipo administrado
- AgileSquads
- Servicio de consultoría

### Presencia
- [Facebook](https://www.facebook.com/SvitlaSystems)
- [Twitter](https://twitter.com/SvitlaSystemsIn)  

### Ofertas laborales
[Trabajos](https://svitla.com/career)
### Blog
[Blog](https://svitla.com/blog)
### Tecnologías
- Java
- React.js
- Node.js
- CSS3, HTML5, JavaScript y frameworks JavaScript como jQuery, React / Preact, TypeScript, etc.
- Ruby
- Python
- QA
- DevOps

## michelada.io
![Logo michelada.io](imagenes/michelada-io.png)
### Link
[michelada.io](https://www.michelada.io/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/michelada.io/@19.266788,-103.717539,15z/data=!4m5!3m4!1s0x0:0x3dbaa3579153065f!8m2!3d19.266788!4d-103.717539?hl=es-419)
### Acerca de
Planificamos, codificamos y lanzamos increíbles productos web y móviles.
### Servicios 
- Desarrollo web
- Desarrollo móvil
- Incrementar equipos
- Mejorar productos
### Presencia
- [Facebook](https://www.facebook.com/micheladaio)
- [Twitter](https://twitter.com/micheladaio)
- [GitHub](https://github.com/michelada)
### Ofertas laborales
[Trabajos](https://jobs.michelada.io/)
### Blog
[Blog](https://twitter.com/micheladaio)
### Tecnologías
- Ruby on Rails
- Kotlin
- Swift
- JavaScript
- CSS3
- HTML5

## densitylabs
![Logo densitylabs](imagenes/densitylabs.svg)
### Link
[densitylabs](https://densitylabs.io)
### Ubicación
[Ubicación](https://goo.gl/maps/TDtAHXpXeuGcKPCh9)
### Acerca de
Density Labs es una empresa de desarrollo de productos de tecnología que se especializa en el desarrollo rápido de aplicaciones de software para móviles, web, SaaS y empresariales.  
### Servicios 
- Desarrollo de software  
- Control de calidad y automatización  
- DevOps  
- Gestión de proyectos  
- Diseño UI/UX  

### Presencia
- [Twitter](https://twitter.com/densitylabs)  
- [Facebook](https://www.facebook.com/densitylabs)  
- [Youtube](https://www.youtube.com/channel/UCk5QUrs2j63QpC8JjQ8yr-A)  

### Ofertas laborales
[Trabajos](https://densitylabs.io/careers)
### Blog
[Blog](https://densitylabs.io/blog)
### Tecnologías
- Ruby  
- React  
- Node  
- JavaScript  
- HTML  
- CSS  
- SASS  
- Redux  
- API UI/UX  

## EDENRED
![Logo edenred](imagenes/des.png)
### Link
[Edenred](https://www.edenred.mx/)
### Ubicación
[Ubicación](https://www.google.com/maps/place/Edenred+M%C3%A9xico+S.A.+De+C.V./@19.439292,-99.192705,16z/data=!4m9!1m2!2m1!1sedenred!3m5!1s0x85d1f8a9d42d3731:0x18a22f0a850580ad!8m2!3d19.4391778!4d-99.1897505!15sCgdlZGVucmVkkgEVZmluYW5jaWFsX2luc3RpdHV0aW9u)
### Acerca de
Edenred es una plataforma líder de pagos y servicios, el compañero diario de las personas en el trabajo en 46 países. El Grupo Edenred cree firmemente en el crecimiento, es por ello que se esfuerza por ser un líder digital responsable y comprometido con ayudar a los trabajadores, empresas, comercios, autoridades públicas y comunidades locales.
Conectando a 50 millones de usuarios y 2 millones de socios comerciales a través de más de 850,000 clientes corporativos.
### Servicios 
- Teletrabajo
### Presencia
- [Facebook](https://www.facebook.com/EdenredMx)
- [Twitter](https://twitter.com/edenredmexico)
- [Instagram](https://www.instagram.com/edenred_mx/)
- [YouTube](https://www.youtube.com/user/EdenredMX)
### Ofertas laborales
[Trabajos](https://www.linkedin.com/company/edenred-m-xico/)
### Blog
[Blog](https://blog.edenred.mx/)
### Tecnologías
- JavaScript
- PHP
- HTML
- CSS

## INTELIMETRICA
![Logo INTELIMETRICA](imagenes/INTELIMETRICA.png)
### Link
[INTELIMETRICA](https://intelimetrica.com/)
### Ubicación
[Ubicación](https://goo.gl/maps/GT94Tg6qcPCKM1iE9)
### Acerca de
Intelimetrica tiene la capacidad de transformar nuestros desafíos y problemas comerciales en soluciones analíticas avanzadas. Son flexibles a nuestras necesidades comerciales y adaptan fácilmente sus soluciones para satisfacer los requisitos de nuestros clientes.
### Servicios 
- IA
- ML
- Ingeniería de software
- Diseño UX
### Presencia
- [Facebook](https://www.facebook.com/intelimetrica)
- [Twitter](https://twitter.com/Intelimetrica)
### Ofertas laborales
[Trabajos](https://intelimetrica.com/careers)
### Blog
[Blog](https://intelimetrica.com/news)
### Tecnologías
- Python, R, Matlab, Java, C, etc
- SQL database systems
- CI/CD, Docker and web scraping
- Frameworks (Flask, FastAPI, Phoenix)
- Software design (SOLID, KISS, DRY, YAGNI)

## contpaqi
![Logo contpaqi.io](imagenes/conta.png)
### Link
[contpaqi](https://www.contpaqi.com/)
### Ubicación
[Ubicación](https://www.google.com/search?tbs=lf:1,lf_ui:4&tbm=lcl&sxsrf=AOaemvL0tXPNxmrFj67xdvIYP0UyAosXYA:1634180929893&q=contpaqi&rflfq=1&num=10&ved=2ahUKEwico8yu9sjzAhVT_J4KHRdKBOAQtgN6BAgKEAQ#rlfi=hd:;si:14952358655611393936;mv:[[19.176002308711503,-96.08430934725374],[19.11478439117776,-96.12825465975374],null,[19.145396188418637,-96.10628200350374],14])
### Acerca de
SOBRE NOSOTROS
En CONTPAQi® somos pioneros en software contable y administrativo. Nuestro objetivo es impulsar a las empresas y profesionales a lo largo de México.
### Servicios 
- Procesos contables
- Procesos comerciales
- Productividad
- Nube
- WOPEN
### Presencia
- [Facebook](https://www.facebook.com/CONTPAQi)
- [Twitter](https://twitter.com/CONTPAQi)
- [YouTube](https://www.youtube.com/contpaqi1)
### Ofertas laborales
[Bolsa de trabajo](https://www.linkedin.com/company/contpaqi1)
### Blog
[Blog](https://blog.contpaqi.com/)
### Tecnologías
- Azure
- JavaScript
- CSS
- HTML
- JAVA
- MySQL

